mod config;

use std::{
    error::Error,
    fs::{create_dir_all, remove_dir_all, rename, File},
    io::{copy, stdin, Cursor},
    path::{Path, PathBuf},
};
use std::fs::read_to_string;

use directories::BaseDirs;
use semver::Version;
use zip::ZipArchive;
use crate::config::Config;

fn main() -> Result<(), Box<dyn Error>> {
    let config = toml::from_str::<Config>(&read_to_string("Config.toml")?)?;

    // Find the BYAR engine directory
    let app_data_path = BaseDirs::new()
        .unwrap()
        .data_local_dir()
        .join("Programs/Beyond-All-Reason");
    let program_files_path = Path::new("C:/Program Files/Beyond-All-Reason");

    let mut engine_directories_opt = None;

    while engine_directories_opt.is_none() {
        if app_data_path.join(&config.test_file).exists() {
            println!("Found Beyond All Reason directory in AppData");
            engine_directories_opt = Some(app_data_path.join("data/engine"));
        } else if program_files_path.join(&config.test_file).exists() {
            println!("Found Beyond All Reason directory in Program Files");
            engine_directories_opt = Some(program_files_path.join("data/engine"));
        } else {
            println!("Could not find Beyond All Reason directory, please input the directory containing {}", config.test_file);

            let mut input_dir = String::new();
            stdin().read_line(&mut input_dir)?;
            let input_path = Path::new(&input_dir);

            if input_path.join(&config.test_file).exists() {
                println!("Found Beyond All Reason directory in input path");
                engine_directories_opt = Some(input_path.join("data/engine"));
            }
        }
    }

    let engine_directories = engine_directories_opt.unwrap();

    let mut engine_current_version = Version::new(0, 0, 0);
    let mut engine_path = PathBuf::new();
    for directory_res in engine_directories.read_dir()? {
        let directory = directory_res?.path();
        let split_name = directory
            .file_name()
            .unwrap()
            .to_string_lossy()
            .split(' ')
            .collect::<Vec<_>>()[0]
            .to_string();
        let engine_version = Version::parse(&split_name)?;

        if engine_version > engine_current_version {
            engine_current_version = engine_version;
            engine_path = directory;
        }
    }

    println!(
        "Most recent engine is: {}",
        engine_path.file_name().unwrap().to_string_lossy()
    );

    let ai_directory = engine_path.join("AI/Skirmish");

    let install = if ai_directory.join(&config.ai_name).exists() {
        println!("CRustAIcean already installed.  Should it be:\n\t[1] Overwritten/Updated\n\t[2] Uninstalled");

        let mut input_choice = String::new();
        stdin().read_line(&mut input_choice)?;

        input_choice.trim().parse::<usize>().unwrap_or(1) == 1
    } else {
        true
    };

    if install {
        println!("Installing {}", config.ai_name);
    } else {
        println!("Uninstalling {}", config.ai_name);
    }

    if ai_directory.join(&config.ai_name).exists() {
        println!("Removing installed version of {}", config.ai_name);
        remove_dir_all(ai_directory.join(&config.ai_name))?;
    }

    if install {
        if !ai_directory.join("backup/NullAI").exists() {
            println!("Creating NullAI backup");
            create_dir_all(ai_directory.join("backup"))?;
            rename(
                ai_directory.join("NullAI"),
                ai_directory.join("backup/NullAI"),
            )?;
        }

        // Download the most current CRustAIcean
        let release_url = if config.download_url_exact {config.download_url.clone()} else {
            let target_latest = config.download_url;

            println!("Locating latest release at: {target_latest}");

            let response_latest = reqwest::blocking::get(target_latest)?;
            let text_latest = response_latest.text()?;

            text_latest.split("url=").collect::<Vec<_>>()[1]
                .split("\">")
                .collect::<Vec<_>>()[0].to_string()
        };
        println!("Downloading latest release from: {release_url}");
        let release_response = reqwest::blocking::get(release_url)?;

        let content = release_response.bytes()?.to_vec();

        let mut archive = ZipArchive::new(Cursor::new(content))?;

        let tmp_dir = tempfile::Builder::new().prefix("example").tempdir()?;

        println!(
            "Creating temporary directory at: {}",
            tmp_dir.path().to_string_lossy()
        );

        for i in 0..archive.len() {
            let mut file = archive.by_index(i)?;

            if file.is_dir() {
                println!(
                    "Extracting directory: {}",
                    tmp_dir.path().join(file.name()).to_string_lossy()
                );
                create_dir_all(tmp_dir.path().join(file.name()))?;
            } else {
                println!(
                    "Extracting file: {}",
                    tmp_dir.path().join(file.name()).to_string_lossy()
                );

                create_dir_all(tmp_dir.path().join(file.name()).parent().unwrap())?;

                let mut dest = {
                    let fname = tmp_dir.path().join(tmp_dir.path().join(file.name()));
                    File::create(fname)?
                };

                copy(&mut file, &mut dest)?;
            }
        }

        println!("Copying {} into directory", config.ai_name);
        rename(
            tmp_dir.path().join("final"),
            ai_directory.join(&config.ai_name),
        )?;
    } else {
        if ai_directory.join("backup/NullAI").exists() {
            println!("Restoring NullAI from backup");
            rename(
                ai_directory.join("backup/NullAI"),
                ai_directory.join("NullAI"),
            )?;
        }
    }

    println!("Finished!");

    println!("Press [Enter] to exit.");
    stdin().read_line(&mut String::new())?;

    Ok(())
}
