use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Config {
    pub ai_name: String,
    pub download_url: String,
    pub download_url_exact: bool,
    pub test_file: String,
}